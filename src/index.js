const wrapper = createEl('div');
wrapper.classList.add('wrapper');

const header = createEl('div');
header.classList.add('header');

const title = createEl('span');
title.classList.add('header-title');
title.innerText = 'INFINITE SCROLL'

header.append(title);

const listOfCryptoCoins = createEl('div');
listOfCryptoCoins.classList.add('crypto-list')


const scrollArea = createEl('div');
scrollArea.setAttribute('id', 'scroll-area');

listOfCryptoCoins.append(scrollArea);

wrapper.append(header, listOfCryptoCoins);

document.body.prepend(wrapper);

let page = 1;

const options = {
    root: document.querySelector('.crypto-list'),
    rootMargin: '0px',
    threshold: 1.0
};

const callback = function(entries, observer) {
    if (entries[0].intersectionRatio <= 0) return;

    loadListItems(page++);
};

const observer = new IntersectionObserver(callback, options);

observer.observe(scrollArea);

async function loadListItems(page, size) {
    // const apiEndpoint = 'https://random-data-api.com/api/crypto_coin/random_crypto_coin';
    const apiEndpoint = 'https://jsonplaceholder.typicode.com/posts';
    const fetchedListItems = await fetchData(page, size)(apiEndpoint);

    console.log(fetchedListItems);

    const fragment = document.createDocumentFragment();

    fetchedListItems.forEach(li => {
        const { id, body, title } = li;
        const listItem = document.createElement('div');
        listItem.classList.add('crypto-list-item');

        listItem.textContent = `${id}: ${body}`

        fragment.appendChild(listItem);
    });

    setTimeout(() => {
        scrollArea.before(fragment);
    }, 2000);
    
}

function fetchData(page = 1, size = 10) {
    let _page = page;
    let _size = size;

    return function(url, next = false) {
        const searchParams = [];

        if(_page !== null && !next) {
            searchParams.push(`_page=${_page}`);
        } else {
            _page++;
            searchParams.push(`_page=${_page}`);
        }

        if(_size !== null) searchParams.push(`_limit=${_size}`);

        return fetch(`${url}?${searchParams.join('&')}`)
                .then(res => res.json())
                .catch(err => err);
    }
}


function createEl(element) {
    const _el = document.createElement(element);

    return _el;
}